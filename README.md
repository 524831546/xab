# xab

#### 介绍
这是一个golang版，用于进行web服务性能压测的工具，类似ab、webench之类的工具，比这两个工具多一个支持压测URL列表的功能。

#### 软件架构
软件架构说明


#### 安装教程

1. 下载xab.zip 解压到/usr/local/bin目录下。
2. 备份/etc/pki/tls/certs/ca-bundle.crt。
3. 下载cacert.pem 并替换掉/etc/pki/tls/certs/ca-bundle.crt。【又或者去https://curl.haxx.se/docs/caextract.html 下载cacert.pem】
4. ![参数说明](https://images.gitee.com/uploads/images/2019/0513/110000_bc2bb024_979382.png "help.png")

#### 使用说明

1. `xab -c 8 https://www.baidu.com/favico.ico` 同时8个并发去请求百度的ico文件
2. `xab -c 3 -t 60 https://www.baidu.com/favico.ico` 同时3个并发去访问ico文件，持续访问60秒。
3. `xab -n 10 https://www.baidu.com/favico.ico` 一个并发循环访问ico文件10次。
4. `xab -x 192.168.x.x:8008 -n 10 https://www.baidu.com/favico.ico` 设置代理通过代理192.168.x.x:8008 访问ico文件10次
5. `xab -ca /root/cacert.pem -n 10 https://www.baidu.com/favico.ico` 指定证书访问ico文件10次。不指定证书位置默认读取/etc/pki/tls/certs/ca-bundle.crt
6. `xab -c 5 -t 120 url.txt` 表示将url.txt中的url列表按照5个并发进行压测，压测120秒。url.txt中的URL格式为一行一个URL。

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)